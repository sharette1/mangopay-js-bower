declare var mangoPay: mangopay.IMangopayStatic;

declare module mangopay {

  export interface IMangopayStatic {

    cardRegistration: {

      baseURL: string;
      clientId: string;
      init: (cardRegisterData: InitOptions) => void;
      registerCard: (cardData: CardData, successCallback: (reg: CardRegistration) => void, errorCallback: (err: any) => void) => void;
      _validateCardData: (cardData: CardData) => any;

    }

  }

  export interface InitOptions {
    Id: string;
    cardRegistrationURL: string;
    preregistrationData: string;
    accessKey: string;
  }

  export interface CardData {

    cardNumber: string;
    cardType: string;
    cardExpirationDate: string;
    cardCvx: string;

  }

  export interface CardRegistration {
    Id: string;
    Tag: string;
    CreationDate: number;
    UserId: string;
    AccessKey: string;
    PreregistrationData: string;
    RegistrationData: string;
    CardId: string;
    CardRegistrationURL: string;
    ResultCode: string;
    ResultMessage: string;
    Currency: string;
    Status: string;
  }

  export interface Error {
    ResultCode: string;
    ResultMessage: string;
  }

}
